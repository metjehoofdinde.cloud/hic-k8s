output "host" {
  value = "${azurerm_kubernetes_cluster.hic.fqdn}"
}

output "pv-stoa-name" {
  value = "${azurerm_storage_account.k8s-pv.name}"
}

output "static-ip-address" {
  value = "${azurerm_public_ip.k8s-pip.ip_address}"
}
