variable "resource_prefix" {
  type    = "string"
  default = "hic"
}

variable "location" {
  type    = "string"
  default = "west europe"
}

variable "tags" {
  type = "map"

  default = {
    terrafrommanaged = "true"
  }
}

variable "agent_pool_profile" {
  type = "map"

  default = {
    name            = "default"
    count           = 2
    vm_size         = "Standard_A1_v2"
    os_type         = "Linux"
    os_disk_size_gb = 30
  }
}

variable "k8s_service_principal_client_id" {
  type = "string"

  default = "00000000-0000-0000-0000-000000000000"
}

variable "k8s_service_principal_object_id" {
  type = "string"

  default = "00000000-0000-0000-0000-000000000000"
}

variable "k8s_service_principal_client_secret" {
  type = "string"

  default = "00000000000000000000000000000000"
}
