resource "azurerm_resource_group" "k8s" {
  name     = "${var.resource_prefix}-k8s"
  location = "${var.location}"

  tags = "${var.tags}"
}

resource "azurerm_role_assignment" "k8s-sp" {
  scope                = "${azurerm_resource_group.k8s.id}"
  role_definition_name = "Contributor"
  principal_id         = "${var.k8s_service_principal_object_id}"
}

resource "azurerm_kubernetes_cluster" "hic" {
  name                = "${var.resource_prefix}-k8s-aks"
  location            = "${azurerm_resource_group.k8s.location}"
  resource_group_name = "${azurerm_resource_group.k8s.name}"
  dns_prefix          = "${var.resource_prefix}-k8s-aks"

  linux_profile {
    admin_username = "sys_host_k8s"

    ssh_key {
      key_data = "${file("${path.module}/../keys/sys_host_k8s.pub")}"
    }
  }

  agent_pool_profile = ["${var.agent_pool_profile}"]

  service_principal {
    client_id     = "${var.k8s_service_principal_client_id}"
    client_secret = "${var.k8s_service_principal_client_secret}"
  }

  tags = "${var.tags}"
}

# need to create the persistent volume storage acocunt in the secondary resource group.
# https://docs.microsoft.com/en-us/azure/aks/faq#why-are-two-resource-groups-created-with-aks 
# https://docs.microsoft.com/en-us/azure/aks/azure-files-dynamic-pv#create-storage-account
data "azurerm_resource_group" "k8s-autocreated" {
  name = "MC_${azurerm_resource_group.k8s.name}_${azurerm_kubernetes_cluster.hic.name}_${azurerm_resource_group.k8s.location}"
}

resource "azurerm_storage_account" "k8s-pv" {
  name                     = "${var.resource_prefix}k8spv"
  location                 = "${data.azurerm_resource_group.k8s-autocreated.location}"
  resource_group_name      = "${data.azurerm_resource_group.k8s-autocreated.name}"
  account_tier             = "Standard"
  account_replication_type = "LRS"

  tags = "${var.tags}"
}

resource "azurerm_public_ip" "k8s-pip" {
  name                         = "${azurerm_kubernetes_cluster.hic.dns_prefix}-pip"
  location                     = "${data.azurerm_resource_group.k8s-autocreated.location}"
  resource_group_name          = "${data.azurerm_resource_group.k8s-autocreated.name}"
  public_ip_address_allocation = "static"

  domain_name_label = "${azurerm_kubernetes_cluster.hic.dns_prefix}-pip"

  tags {
    environment = "Production"
  }
}
